

/**
 * @author		Nicholas Smith - nicholas.smith@online.liverpool.ac.uk
 * @version	1.0.0.0
 * @since		2015-10-03
 * 
 */
public class MicroIllustratorMain
{
	public static void main(String[] args)
	{
		MicroIllustrator microIllustrator = new MicroIllustrator();
		microIllustrator.launch();
		
	}
}