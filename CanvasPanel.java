import java.awt.*;
import java.awt.event.*;
import javax.swing.*;


/**
 * @author		Nicholas Smith - nicholas.smith@online.liverpool.ac.uk
 * @version	1.0.0.0
 * @since		2015-10-05
 * 
 */
public class CanvasPanel extends JPanel implements MouseListener, MouseMotionListener
{
	private int diameter;
	private String selectedShapeCode;
	private boolean fillSelectedShape;
	private Color selectedShapeOutlineColor;
	private Color selectedShapeFillColor;
	
	
	private boolean isDragging;
	private int originPointX;
	private int originPointY;
	private int endPointX;
	private int endPointY;
	
	
	private CanvasShapeList canvasShapeList;
	
	
	public CanvasPanel()
	{
		diameter = 10;
		selectedShapeCode = "line";
		fillSelectedShape = false;
		this.selectedShapeOutlineColor = Color.BLACK;
		this.selectedShapeFillColor = Color.BLACK;
		
		isDragging = false;
		originPointX = -1;
		originPointY = -1;
		endPointX = -1;
		endPointY = -1;
	
		canvasShapeList = new CanvasShapeList();
		
		addMouseListener(this);
		addMouseMotionListener(this);
						
	}
	
	
	public int getDiameter()
	{
		return this.diameter;
		
	}
	
	public String getSelectedShapeCode()
	{
		return this.selectedShapeCode;
		
	}
	
	public boolean getFillSelectedShape()
	{
		return this.fillSelectedShape;
	
	}
	
	public Color getSelectedShapeOutlineColor()
	{
		return this.selectedShapeOutlineColor;
	
	}
	
	public Color getSelectedShapeFillColor()
	{
		return this.selectedShapeFillColor;
	
	}
	
	public CanvasShapeList getCanvasShapeList()
	{
		return this.canvasShapeList;
		
	}
	
	public boolean getIsDragging()
	{
		return this.isDragging;
		
	}
	
	public int getOriginPointX()
	{
		return this.originPointX;
		
	}
	
	public int getOriginPointY()
	{
		return this.originPointY;
		
	}
	
	public int getEndPointX()
	{
		return this.endPointX;
		
	}
	
	public int getEndPointY()
	{
		return this.endPointY;
		
	}

	
	
	public void setDiameter(int diameter)
	{
		this.diameter = diameter;
		
	}
	
	public void setSelectedShapeCode(String selectedShapeCode)
	{
		this.selectedShapeCode = selectedShapeCode;
		
	}

	public void setFillSelectedShape(boolean fillSelectedShape)
	{
		this.fillSelectedShape = fillSelectedShape;
		
	}
	
	public void setSelectedShapeOutlineColor(Color selectedShapeOutlineColor)
	{
		this.selectedShapeOutlineColor = selectedShapeOutlineColor;
	
	}
	
	public void setSelectedShapeFillColor(Color selectedShapeFillColor)
	{
		this.selectedShapeFillColor = selectedShapeFillColor;
	
	}
	
	public void setCanvasShapeList(CanvasShapeList canvasShapeList)
	{
		this.canvasShapeList = canvasShapeList;
		
	}
	
	public void setIsDragging(boolean isDragging)
	{
		this.isDragging = isDragging;
		
	}
	
	public void setOriginPointX(int originPointX)
	{
		this.originPointX = originPointX;
		
	}
	
	public void setOriginPointY(int originPointY)
	{
		this.originPointY = originPointY;
		
	}
	
	public void setEndPointX(int endPointX)
	{
		this.endPointX = endPointX;
		
	}
	
	public void setEndPointY(int endPointY)
	{
		this.endPointY = endPointY;
		
	}
	
	
	
	
	@Override
	public void paintComponent(Graphics g)
	{
		super.paintComponent(g);		
		loadDrawnShapes(g);
	}
	
	public void loadDrawnShapes(Graphics g)
	{		
		for (int shapeCount = 0; shapeCount < this.canvasShapeList.getSize(); shapeCount++)
		{			
			processDrawShapeRequest(g, this.canvasShapeList.getShape(shapeCount));
		}
		
	}
	
	
	public void processDrawShapeRequest(Graphics g, MyShapeAbstract shape)
	{
		String selectedShapeLookupCode = shape.getShapeLookupCode().toLowerCase();
		int[] drawingPointsData = shape.getDrawingPointsData();
		
		
		if (shape.getFillShape())
		{
			g.setColor(shape.getShapeColor());
		}
		else
		{					
			g.setColor(shape.getShapeColor());
		}
		
				
		switch (selectedShapeLookupCode)
		{
			case "line":
				g.drawLine(drawingPointsData[0], drawingPointsData[1], drawingPointsData[2], drawingPointsData[3]);
			
				break;
				
			case "oval":
				if (shape.getFillShape())
				{
					g.fillOval(drawingPointsData[0], drawingPointsData[1], drawingPointsData[2], drawingPointsData[3]);
				}
				else
				{					
					g.drawOval(drawingPointsData[0], drawingPointsData[1], drawingPointsData[2], drawingPointsData[3]);
				}
				
				break;
				
			case "rectangle":
				if (shape.getFillShape())
				{
					g.fillRect(drawingPointsData[0], drawingPointsData[1], drawingPointsData[2], drawingPointsData[3]);
				}
				else
				{
					g.drawRect(drawingPointsData[0], drawingPointsData[1], drawingPointsData[2], drawingPointsData[3]);
				}
				
				break;
				
			default:
				
		}
	}
	
	
	
	public void addDrawnShape(int originPointX, int originPointY, int width_endPointX, int height_endPointY, String shapeType, Color shapeColor, boolean fillShape)
	{
		this.canvasShapeList.add(originPointX, originPointY, width_endPointX, height_endPointY, shapeType, shapeColor, fillShape);
	}
	

	
	@Override
	public void mouseMoved(MouseEvent e) {}
	
	@Override
    public void mouseDragged(MouseEvent e) {
		Point point = e.getPoint();
		
		endPointX = point.x;
		endPointY = point.y;
		
		if (isDragging)
		{
			repaint();
		}
		
    }
	
	@Override
	public void mousePressed(MouseEvent e)
	{
		Point point = e.getPoint();
		
		originPointX = point.x;
		originPointY = point.y;
		
		this.isDragging = true;
	}
	
	@Override
	public void mouseReleased(MouseEvent e)
	{
		String shapeCode = this.selectedShapeCode.toLowerCase();
		int shapeHeight = 0;
		int shapeWidth = 0;
		
		Color shapeColor;
		
		this.isDragging = false;
		
		
		if (this.fillSelectedShape)
		{
			shapeColor = this.selectedShapeFillColor;
		}
		else
		{
			shapeColor = this.selectedShapeOutlineColor;
		}
		
		
		switch (shapeCode)
		{
			case "line":
				addDrawnShape(this.originPointX, this.originPointY, this.endPointX, this.endPointY, this.selectedShapeCode, this.selectedShapeOutlineColor, this.fillSelectedShape);
			
				break;
				
			case "oval":
				shapeHeight = this.endPointY - this.originPointY;
				shapeWidth = this.endPointX - this.originPointX;
		
				addDrawnShape(this.originPointX, this.originPointY, shapeWidth, shapeHeight, this.selectedShapeCode, shapeColor, this.fillSelectedShape);
				
				break;
				
			case "rectangle":
				shapeHeight = this.endPointY - this.originPointY;
				shapeWidth = this.endPointX - this.originPointX;
				
				addDrawnShape(this.originPointX, this.originPointY, shapeWidth, shapeHeight, this.selectedShapeCode, shapeColor, this.fillSelectedShape);
				
				break;
				
			default:
				
		}	

		repaint();
		
	}
	
	@Override
	public void mouseClicked(MouseEvent e) {}	
	
	@Override
	public void mouseEntered(MouseEvent e) {}
	
	@Override
	public void mouseExited(MouseEvent e) {}
	
}