import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

/**
 * @author		Nicholas Smith - nicholas.smith@online.liverpool.ac.uk
 * @version	1.0.0.0
 * @since		2015-10-03
 * 
 */
public abstract class WindowFrameAbstract
{
	private String menuName;
	private JFrame menuFrame;
	
	public WindowFrameAbstract()
	{
		prepareGUI("Menu Name", "App", 400, 600);
	}
	
	public WindowFrameAbstract(String menuName, String frameTitle, int width, int height)
	{
		prepareGUI(menuName, frameTitle, width, height);
	}
	
	
	public void prepareGUI(String menuName, String frameTitle, int width, int height)
	{
		this.menuName = menuName;
		
		menuFrame = new JFrame(frameTitle);
		menuFrame.setSize(width, height);
		menuFrame.setLayout(new GridLayout(1, 1));
		
		menuFrame.addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent windowEvent) {
				System.exit(0);
			}
		});
		
	}
	
	
	public String getMenuName()
	{
		return this.menuName;
	}
	
	public JFrame getMenuFrame()
	{
		return this.menuFrame;
	}
	
	
	
	public void setMenuName(String menuName)
	{
		this.menuName = menuName;
	}
	
	public void setMenuName(JFrame menuFrame)
	{
		this.menuFrame = menuFrame;
	}
	
	
	
	public void show()
	{
		getMenuFrame().setVisible(true);
	}
	
	public void hide()
	{
		getMenuFrame().setVisible(false);
	}
	
	public void close()
	{
		getMenuFrame().dispose();
	}
	
		
	public abstract void build();
	
}