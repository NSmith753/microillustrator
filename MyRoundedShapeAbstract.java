import java.awt.*;
import javax.swing.*;

/**
 * @author		Nicholas Smith - nicholas.smith@online.liverpool.ac.uk
 * @version	1.0.0.0
 * @since		2015-10-05
 * 
 */
public abstract class MyRoundedShapeAbstract extends MyShapeAbstract
{
	private int width;
	private int height;
	
	
	public MyRoundedShapeAbstract()
	{
		super();
		
		this.width = 0;
		this.height = 0;
		
	}
	
	public MyRoundedShapeAbstract(String shapeLookupCode, int originPointX, int originPointY, int width, int height, Color shapeColor, boolean fillShape)
	{
		super(shapeLookupCode, originPointX, originPointY, shapeColor, fillShape);
		
		this.width = width;
		this.height = height;
		
	}
	
	
	public int getWidth()
	{
		return this.width;
	}
	
	public int getHeight()
	{
		return this.height;
	}
	
	
	
	public void setWidth(int width)
	{
		this.width = width;
	}
	
	public void setHeight(int height)
	{
		this.height = height;
	}
	
}