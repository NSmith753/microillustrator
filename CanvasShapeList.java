import java.util.*;
import java.awt.Color;
import javax.swing.*;


/**
 * @author		Nicholas Smith - nicholas.smith@online.liverpool.ac.uk
 * @version	1.0.0.0
 * @since		2015-10-04
 * 
 */

public class CanvasShapeList
{
	private List<MyShapeAbstract> shapeList = new ArrayList<MyShapeAbstract>();
	
	
	public void setShapeList(List<MyShapeAbstract> shapeList)
	{
		this.shapeList = shapeList;
	}
	
	public List<MyShapeAbstract> getShapeList()
	{
		return this.shapeList;
	}
	
	public MyShapeAbstract getShape(int shapePosition)
	{
		return this.shapeList.get(shapePosition);
	}
	
	public MyShapeAbstract getShape(int coordinateX, int coordinateY)
	{
		return getShape(findShape(coordinateX, coordinateY));
	}
	
	public int getSize()
	{
		return shapeList.size();
	}
	
	public boolean add(int originPointX, int originPointY, int width_endPointX, int height_endPointY, String shapeType, Color shapeColor, boolean fillShape)
	{
		boolean isValid = false;
		MyShapeAbstract shape = processBuildShapeRequest(shapeType, originPointX, originPointY, width_endPointX, height_endPointY, shapeColor, fillShape);
					
		this.shapeList.add(shape);
		
		return isValid;
		
	}
	
	public void remove(int index)
	{
		if (index >= 0 && index < this.shapeList.size())
		{
			this.shapeList.remove(index);
		}	
		
	}
	
	public void removeAll()
	{
		this.shapeList.clear();
	}
	
	public int findShape(int coordinateX, int coordinateY)
	{
		boolean isFound = false;
		int shapePosition = -1;
		int count = 0;
		
		while (count < this.shapeList.size() && isFound != true)
		{ 
			if (this.shapeList.get(count).isWithinShape(coordinateX, coordinateY))
			{
				isFound = true;
				shapePosition = count;
			}
			
			count++;
			
		}
		
		return shapePosition;
		
	}
	
	public MyShapeAbstract processBuildShapeRequest(String shapeType, int originPointX, int originPointY, int width_endPointX, int height_endPointY, Color shapeColor, boolean fillShape)
	{
		shapeType = shapeType.toLowerCase();
		
		MyShapeAbstract shape = null;
		
		switch (shapeType)
		{
			case "line":
				shape = new MyLine(originPointX, originPointY, width_endPointX, height_endPointY, shapeColor, fillShape);
				
				break;
				
				
			case "oval":
				shape = new MyOval(originPointX, originPointY, width_endPointX, height_endPointY, shapeColor, fillShape);
				
				break;
				
				
			case "rectangle":
				shape = new MyRectangle(originPointX, originPointY, width_endPointX, height_endPointY, shapeColor, fillShape);
				
				break;
					
				
			default:
			
		}
		
		return shape;
	}
	
}