import java.awt.*;
import javax.swing.*;



/**
 * @author		Nicholas Smith - nicholas.smith@online.liverpool.ac.uk
 * @version	1.0.0.0
 * @since		2015-10-03
 * 
 */
public class ColorChooserFrame extends MicroIllustratorFrameAbstract
{
	private ColorChooserPanel colorChooserPanel;
	private Color selectedColor;
	
	
	public ColorChooserFrame()
	{
		super();
		
		colorChooserPanel = new ColorChooserPanel();
		selectedColor = new Color(255, 255, 255);
		
	}
	
	public ColorChooserFrame(String menuName, String frameTitle, int width, int height)
	{
		super(menuName, frameTitle, width, height);
		
		colorChooserPanel = new ColorChooserPanel();
		selectedColor = new Color(255, 255, 255);
		
	}
	
	public ColorChooserPanel getColorChooserPanel()
	{
		return this.colorChooserPanel;
	}
	
	public Color getSelectedColor()
	{
		return this.selectedColor;
	}
	
	
	public void setColorChooserPanel(ColorChooserPanel colorChooserPanel)
	{
		this.colorChooserPanel = colorChooserPanel;
	}
	
	public void setSelectedColor(Color selectedColor)
	{
		this.selectedColor = selectedColor;
	}
	
	
	public void build()
	{
		setSelectedColor(colorChooserPanel.getSelectedColor());
	}
		
	public void show()
	{
		String selectedColorType = getSelectedColorType().toLowerCase();
		
		this.colorChooserPanel.launchColorChooser();
		
		switch (selectedColorType)
		{
			case "outline":
				setSelectedOutlineColor(this.colorChooserPanel.getSelectedColor());
				
				break;
			
			case "fill":
				setSelectedFillColor(this.colorChooserPanel.getSelectedColor());
				
				break;
			
			default:
			
		}
		
	}
		
}