# README #

### What is this repository for? ###

A program for drawing simple shapes on a canvas much like MS Paint. It uses a Java Swing GUI to allow the user to draw and select shapes and their colour properties by click and dragging the mouse cursor over the white canvas in the application window.

* Version
v1.0.0.0


### How do I get set up? ###

Download a copy, cd to the directory in terminal, compile using ```javac *.java ``` and then run using the ```java MicroIllustratorMain ``` command.
Built for Java SE 8