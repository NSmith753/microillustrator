import java.awt.*;
import java.awt.event.*;
import javax.swing.*;


/**
 * @author		Nicholas Smith - nicholas.smith@online.liverpool.ac.uk
 * @version	1.0.0.0
 * @since		2015-10-05
 * 
 */
public class MicroIllustratorCanvasFrame extends MicroIllustratorFrameAbstract
{
	private JMenuBar canvasMenuBar;
	private JRadioButtonMenuItem[] selectShapeMenuItem;
	private CanvasPanel canvas;
	private JPanel statusPanel;
	
	private JLabel canvasSizeLabel;
	private JLabel cursorPositionLabel;
	private JLabel selectedColourLabel;
	private JLabel selectedShapeLabel;
	
	private HelperLib helperLib;
	
	
	public MicroIllustratorCanvasFrame()
	{
		super();
		
		helperLib = new HelperLib();
	}
	
	public MicroIllustratorCanvasFrame(String menuName, String frameTitle, int width, int height)
	{
		super(menuName, frameTitle, width, height);
		
		helperLib = new HelperLib();
	}
	
	
	
	public JMenuBar getCanvasMenuBar()
	{
		return this.canvasMenuBar;
	}
	
	public CanvasPanel getCanvas()
	{
		return this.canvas;
	}
	
	public JPanel getStatusPanel()
	{
		return this.statusPanel;
	}
	
	public JLabel getCanvasSizeLabel()
	{
		return this.canvasSizeLabel;
	}
	
	public JLabel getCursorPositionLabel()
	{
		return this.cursorPositionLabel;
	}
	
	public JLabel getSelectedColourLabel()
	{
		return this.selectedColourLabel;
	}
	
	public JLabel getSelectedShapeLabel()
	{
		return this.selectedShapeLabel;
	}
	
	public HelperLib getHelperLib()
	{
		return this.helperLib;
	}
	
	
	
	public void setCanvasMenuBar(JMenuBar canvasMenuBar)
	{
		this.canvasMenuBar = canvasMenuBar;
	}
	
	public void setCanvas(CanvasPanel canvas)
	{
		this.canvas = canvas;
	}
	
	public void setStatusPanel(JPanel statusPanel)
	{
		this.statusPanel = statusPanel;
	}
	
	public void setCanvasSizeLabel(JLabel canvasSizeLabel)
	{
		this.canvasSizeLabel = canvasSizeLabel;
	}
	
	public void setCursorPositionLabel(JLabel cursorPositionLabel)
	{
		this.cursorPositionLabel = cursorPositionLabel;
	}
	
	public void setSelectedColourLabel(JLabel selectedColourLabel)
	{
		this.selectedColourLabel = selectedColourLabel;
	}
	
	public void setSelectedShapeLabel(JLabel selectedShapeLabel)
	{
		this.selectedShapeLabel = selectedShapeLabel;
	}
	
	public void setHelperLib(HelperLib helperLib)
	{
		this.helperLib = helperLib;
	}
	
	
	
	public void buildCanvasMenuBar()
	{
		JMenu fileMenu;
		JMenuItem aboutMenuItem;
		JMenuItem closeMenuItem;
		JMenu drawMenu;
		JMenu selectShapeMenu;
		JMenuItem selectOutlineColourMenuItem;
		JMenuItem selectFillColourMenuItem;
		JCheckBoxMenuItem fillShapeMenuItem;
		MenuItemHandler menuItemHandler = new MenuItemHandler();
		
		this.canvasMenuBar = new JMenuBar();
		
		fileMenu = new JMenu("File");
		fileMenu.setMnemonic('F');		
		
		aboutMenuItem = new JMenuItem("About");
		aboutMenuItem.setMnemonic('A');
		aboutMenuItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String message = "A simple shape drawing application.";
				String title = "microIllustrator";
				
				helperLib.displayMsg(message, title);
				
			}
			
		});
		
		closeMenuItem = new JMenuItem("Close");
		closeMenuItem.setMnemonic('C');
		closeMenuItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				System.exit(0);
			}
			
		});
		
		
		fileMenu.add(aboutMenuItem);
		fileMenu.add(closeMenuItem);
		
		
		
		drawMenu = new JMenu("Draw");
		drawMenu.setMnemonic('D');		
		
		selectShapeMenu = new JMenu("Select Shape");
		selectShapeMenu.setMnemonic('S');
		
		String[] shapeNames = { "Line", "Oval", "Rectangle" };
		
		selectShapeMenuItem = new JRadioButtonMenuItem[shapeNames.length];
		
		for (int shapeCount = 0; shapeCount < shapeNames.length; shapeCount++)
		{
			selectShapeMenuItem[shapeCount] = new JRadioButtonMenuItem(shapeNames[shapeCount]);
			selectShapeMenu.add(selectShapeMenuItem[shapeCount]);
			selectShapeMenuItem[shapeCount].addActionListener(menuItemHandler);
			
		}
		
		selectShapeMenuItem[0].setSelected(true);
		
		
		
		selectOutlineColourMenuItem = new JMenuItem("Select Outline Colour");
		selectOutlineColourMenuItem.setMnemonic('O');
		selectOutlineColourMenuItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				setSelectedColorType("outline");
				getMicroIllustratorGUI().getMenu("colour-chooser").show();
				canvas.setSelectedShapeOutlineColor(getSelectedOutlineColor());
			}
		});
		
		selectFillColourMenuItem = new JMenuItem("Select Fill Colour");
		selectFillColourMenuItem.setMnemonic('I');
		selectFillColourMenuItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				setSelectedColorType("fill");
				getMicroIllustratorGUI().getMenu("colour-chooser").show();
				canvas.setSelectedShapeFillColor(getSelectedFillColor());
			}
		});
		
				
		fillShapeMenuItem = new JCheckBoxMenuItem("Fill Shape");
		fillShapeMenuItem.setMnemonic('L');
		fillShapeMenuItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				AbstractButton aButton = (AbstractButton) e.getSource();
				getCanvas().setFillSelectedShape(aButton.getModel().isSelected());
			}
		}); 

		
		drawMenu.add(selectShapeMenu);
		drawMenu.add(selectOutlineColourMenuItem);
		drawMenu.add(selectFillColourMenuItem);
		drawMenu.add(fillShapeMenuItem);
		
		
		
		
		this.canvasMenuBar.add(fileMenu);
		this.canvasMenuBar.add(drawMenu);
		
	}
	
	
	public void build()
	{
		buildCanvasMenuBar();
		
		this.canvas = new CanvasPanel();
		this.canvas.setLayout(new FlowLayout());
		this.canvas.setBackground(Color.WHITE);
		
		this.statusPanel = new JPanel();
		this.statusPanel.setLayout(new FlowLayout());
		
		
		this.canvasSizeLabel = new JLabel("Canvas Size: set", JLabel.CENTER);
		this.cursorPositionLabel = new JLabel("X: set, Y: set", JLabel.CENTER);
		this.selectedColourLabel = new JLabel("Colour: set", JLabel.CENTER);
		this.selectedShapeLabel = new JLabel("Shape: set", JLabel.CENTER);
		
		
		this.statusPanel.add(this.canvasSizeLabel);
		this.statusPanel.add(this.cursorPositionLabel);
		this.statusPanel.add(this.selectedColourLabel);
		this.statusPanel.add(this.selectedShapeLabel);
		
		
		getMenuFrame().setJMenuBar(this.canvasMenuBar);
		getMenuFrame().add(this.canvas);
		
		
	}
	
	
	private class MenuItemHandler implements ActionListener
	{
		@Override
		public void actionPerformed(ActionEvent e)
		{
			for (int shapeCount = 0; shapeCount < selectShapeMenuItem.length; shapeCount++)
			{
				if (e.getSource() == selectShapeMenuItem[shapeCount])
				{
					canvas.setSelectedShapeCode(selectShapeMenuItem[shapeCount].getText().toLowerCase());
					
				}
				else
				{
					selectShapeMenuItem[shapeCount].setSelected(false);
				}
			}
		}
		
	}
	
}