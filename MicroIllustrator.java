import java.awt.event.*;

/**
 * @author		Nicholas Smith - nicholas.smith@online.liverpool.ac.uk
 * @version	1.0.0.0
 * @since		2015-10-03
 * 
 */
public class MicroIllustrator
{
	public void launch()
	{
		MicroIllustratorCanvasFrame splashPage = new MicroIllustratorCanvasFrame("splash", "microIllustrator", 400, 400);
		splashPage.buildGUI();
		
		splashPage.getMicroIllustratorGUI().getMenu("canvas").show();
		splashPage.close();
		
	}
	
}