import java.awt.*;
import javax.swing.*;


/**
 * @author		Nicholas Smith - nicholas.smith@online.liverpool.ac.uk
 * @version	1.0.0.0
 * @since		2015-10-04
 * 
 */
public abstract class MicroIllustratorFrameAbstract extends WindowFrameAbstract
{
	private static MicroIllustratorGUI microIllustratorGUI = new MicroIllustratorGUI();
	private static Color selectedOutlineColor = new Color(255, 255, 255);
	private static Color selectedFillColor = new Color(255, 255, 255);
	private static String selectedColorType = "";
	
	
	public MicroIllustratorFrameAbstract()
	{
		super();
	}
	
	public MicroIllustratorFrameAbstract(String menuName, String frameTitle, int width, int height)
	{
		super(menuName, frameTitle, width, height);
	}
	
	
	
	public MicroIllustratorGUI getMicroIllustratorGUI()
	{
		return this.microIllustratorGUI;
	}
	
	public Color getSelectedOutlineColor()
	{
		return this.selectedOutlineColor;		
	}
	
	public Color getSelectedFillColor()
	{
		return this.selectedFillColor;		
	}
	
	public String getSelectedColorType()
	{
		return this.selectedColorType;
	}
	
	
	
	public void setMicroIllustratorGUI(MicroIllustratorGUI microIllustratorGUI)
	{
		this.microIllustratorGUI = microIllustratorGUI;
	}	
	
	public void setSelectedOutlineColor(Color selectedOutlineColor)
	{
		this.selectedOutlineColor = selectedOutlineColor;	
	}	
	
	public void setSelectedFillColor(Color selectedFillColor)
	{
		this.selectedFillColor = selectedFillColor;	
	}	
	
	public void setSelectedColorType(String selectedColorType)
	{
		this.selectedColorType = selectedColorType;
	}
	
	
	
	public void buildGUI()
	{
		microIllustratorGUI.add("canvas", "microIllustrator", 900, 800, "canvas");
		microIllustratorGUI.getMenu("canvas").build();
		
		microIllustratorGUI.add("colour-chooser", "microIllustrator", 400, 600, "colour-chooser");
		microIllustratorGUI.getMenu("colour-chooser").build();
		
		microIllustratorGUI.add("shape-selector", "microIllustrator", 400, 600, "shape-selector");
		microIllustratorGUI.getMenu("shape-selector").build();
		
	}
	
}