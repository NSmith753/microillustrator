import java.util.*;


/**
 * @author		Nicholas Smith - nicholas.smith@online.liverpool.ac.uk
 * @version	1.0.0.0
 * @since		2015-10-03
 * 
 */

public class MicroIllustratorGUI
{
	private List<WindowFrameAbstract> menuList = new ArrayList<WindowFrameAbstract>();
	
	
	public void setMenuList(List<WindowFrameAbstract> menuList)
	{
		this.menuList = menuList;
	}
	
	public List<WindowFrameAbstract> getMenuList()
	{
		return this.menuList;
	}
	
	public WindowFrameAbstract getMenu(int menuPosition)
	{
		return this.menuList.get(menuPosition);
	}
	
	public WindowFrameAbstract getMenu(String menuName)
	{
		return getMenu(findMenu(menuName));
	}
	
	public int getSize()
	{
		return menuList.size();
	}
	
	public boolean add(String menuName, String frameTitle, int width, int height, String menuType)
	{
		boolean isValid = false;
		WindowFrameAbstract menuFrame = processBuildMenuRequest(menuType, menuName, frameTitle, width, height);
					
		this.menuList.add(menuFrame);
		
		return isValid;
		
	}
	
	public void remove(int index)
	{
		if (index >= 0 && index < this.menuList.size())
		{
			this.menuList.remove(index);
		}	
		
	}
	
	public void removeAll()
	{
		this.menuList.clear();
	}
	
	public int findMenu(String menuName)
	{
		boolean isFound = false;
		int menuPosition = -1;
		int count = 0;
		
		while (count < this.menuList.size() && isFound != true)
		{ 
			if (this.menuList.get(count).getMenuName().compareTo(menuName) == 0)
			{
				isFound = true;
				menuPosition = count;
			}
			
			count++;
			
		}
		
		return menuPosition;
		
	}
	
	public WindowFrameAbstract processBuildMenuRequest(String menuType, String menuName, String frameTitle, int width, int height)
	{
		menuType = menuType.toLowerCase();
		
		WindowFrameAbstract menuFrame = null;
		
		switch (menuType)
		{
			case "canvas":
				menuFrame = new MicroIllustratorCanvasFrame(menuName, frameTitle, width, height);
				
				break;
				
				
			case "colour-chooser":
				menuFrame = new ColorChooserFrame(menuName, frameTitle, width, height);
				
				break;
				
				
			case "shape-selector":
				menuFrame = new MicroIllustratorCanvasFrame(menuName, frameTitle, width, height);
				
				break;
					
				
			default:
			
		}
		
		return menuFrame;
	}
	
}