import java.awt.*;
import javax.swing.*;

/**
 * @author		Nicholas Smith - nicholas.smith@online.liverpool.ac.uk
 * @version	1.0.0.0
 * @since		2015-10-06
 * 
 */
public class ColorChooserPanel extends JPanel
{
	private Color selectedColor;
    
	public ColorChooserPanel()
	{
        super(new BorderLayout());
		
    }
	
	public Color getSelectedColor()
	{
		return this.selectedColor;
		
	}
	
	public void setSelectedColor(Color selectedColor)
	{
		this.selectedColor = selectedColor;
	
	}

	public void launchColorChooser()
	{
		selectedColor = JColorChooser.showDialog(ColorChooserPanel.this, "Choose Shape Attribute Color", Color.BLACK);
	}
	
}