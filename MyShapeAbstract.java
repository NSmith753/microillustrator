import java.awt.*;
import javax.swing.*;


/**
 * @author		Nicholas Smith - nicholas.smith@online.liverpool.ac.uk
 * @version	1.0.0.0
 * @since		2015-10-03
 * 
 */

public abstract class MyShapeAbstract
{
	private String shapeLookupCode;
	private int originPointX;
	private int originPointY;
	private Color shapeColor;
	private boolean fillShape;
	
	public MyShapeAbstract()
	{
		this.shapeLookupCode = "";
		this.originPointX = 0;
		this.originPointY = 0;
		this.shapeColor = Color.BLACK;
		this.fillShape = false;
	}
	
	public MyShapeAbstract(String shapeLookupCode, int originPointX, int originPointY, Color shapeColor, boolean fillShape)
	{
		this.shapeLookupCode = shapeLookupCode;
		this.originPointX = originPointX;
		this.originPointY = originPointY;
		this.shapeColor = shapeColor;
		this.fillShape = fillShape;
		
	}
	
	
	public String getShapeLookupCode()
	{
		return this.shapeLookupCode;
	}
	
	public int getOriginPointX()
	{
		return this.originPointX;
	}
	
	public int getOriginPointY()
	{
		return this.originPointY;
	}
	
	public Color getShapeColor()
	{
		return this.shapeColor;
	}
	
	public boolean getFillShape()
	{
		return this.fillShape;
	}
	
	
	
	public void setShapeLookupCode(String shapeLookupCode)
	{
		this.shapeLookupCode = shapeLookupCode;
	}
		
	public void setOriginPointX(int originPointX)
	{
		this.originPointX = originPointX;
	}
	
	public void setOriginPointY(int originPointY)
	{
		this.originPointY = originPointY;
	}
	
	public void setShapeColor(Color shapeColor)
	{
		this.shapeColor = shapeColor;
	}
	
	public void setFillShape(boolean fillShape)
	{
		this.fillShape = fillShape;
	}
	
	
	public abstract boolean isWithinShape(int coordinateX, int coordinateY);
	
	public abstract int[] getDrawingPointsData();
	
}