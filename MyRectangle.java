import java.awt.*;
import javax.swing.*;

/**
 * @author		Nicholas Smith - nicholas.smith@online.liverpool.ac.uk
 * @version	1.0.0.0
 * @since		2015-10-05
 * 
 */
public class MyRectangle extends MyRoundedShapeAbstract
{
	public MyRectangle()
	{
		super();
	}
	
	public MyRectangle(int originPointX, int originPointY, int width, int height, Color shapeColor, boolean fillShape)
	{
		super("rectangle", originPointX, originPointY, width, height, shapeColor, fillShape);
	}
	
	public MyRectangle(String shapeLookupCode, int originPointX, int originPointY, int width, int height, Color shapeColor, boolean fillShape)
	{
		super(shapeLookupCode, originPointX, originPointY, width, height, shapeColor, fillShape);
	}
	
	public boolean isWithinShape(int coordinateX, int coordinateY)
	{
		boolean isWithinShape = false;
		
		if (coordinateX >= getOriginPointX() && coordinateX <= (getOriginPointX() + getWidth()))
		{
			if (coordinateY >= getOriginPointY() && coordinateY <= (getOriginPointY() + getHeight()))
			{
				isWithinShape = true;
			}
		}
		
		return isWithinShape;
		
	}
	
	public int[] getDrawingPointsData()
	{
		int[] drawingPointsData = new int[4];
		
		drawingPointsData[0] = getOriginPointX();
		drawingPointsData[1] = getOriginPointY();
		drawingPointsData[2] = getWidth();
		drawingPointsData[3] = getHeight();
		
		return drawingPointsData;
		
	}
	
}