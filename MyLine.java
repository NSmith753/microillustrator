import java.awt.*;
import javax.swing.*;

/**
 * @author		Nicholas Smith - nicholas.smith@online.liverpool.ac.uk
 * @version	1.0.0.0
 * @since		2015-10-05
 * 
 */
public class MyLine extends MyShapeAbstract
{	
	private int endPointX;
	private int endPointY;
	
	public MyLine()
	{
		super("line", 0, 0, Color.BLACK, false);
		
		this.endPointX = 0;
		this.endPointY = 0;
		
	}
	
	public MyLine(int originPointX, int originPointY, int endPointX, int endPointY, Color shapeColor, boolean fillShape)
	{
		super("line", originPointX, originPointY, shapeColor, fillShape);
		
		this.endPointX = endPointX;
		this.endPointY = endPointY;
		
	}
	
	public MyLine(int originPointX, int endPointX, int lineThickness, Color shapeColor, boolean fillShape)
	{
		super("line", originPointX, originPointX + lineThickness, shapeColor, fillShape);
		
		this.endPointX = endPointX;
		this.endPointY = endPointX + lineThickness;
		
	}
		
		
	
	public int getEndPointX()
	{
		return this.endPointX;
	}
	
	public int getEndPointY()
	{
		return this.endPointY;
	}
	
	
	
	public void setEndPointX(int endPointX)
	{
		this.endPointX = endPointX;
	}
	
	public void setEndPointY(int endPointY)
	{
		this.endPointY = endPointY;
	}
	
	
	public boolean isWithinShape(int coordinateX, int coordinateY)
	{
		boolean isWithinShape = false;
		
		if (coordinateX >= getOriginPointX() && coordinateX <= getEndPointX())
		{
			if (coordinateY >= getOriginPointY() && coordinateY <= getEndPointY())
			{
				isWithinShape = true;
			}
		}
		
		return isWithinShape;
		
	}
	
	public int[] getDrawingPointsData()
	{
		int[] drawingPointsData = new int[4];
		
		drawingPointsData[0] = getOriginPointX();
		drawingPointsData[1] = getOriginPointY();
		drawingPointsData[2] = getEndPointX();
		drawingPointsData[3] = getEndPointY();
		
		return drawingPointsData;
		
	}
	
}